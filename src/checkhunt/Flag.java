package checkhunt;

/**
 *
 * @author Oliver
 */
public class Flag {

    private String answer;
    private int points;

    public Flag(String answer, int points) {
        this.answer = answer;
        this.points = points;
    }

    protected String getAnswer() {
        return answer;
    }

    protected int getPoints() {
        return points;
    }
}
