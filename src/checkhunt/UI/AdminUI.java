package checkhunt.UI;

import checkhunt.CheckHunt;
import checkhunt.Contestant;
import checkhunt.Prefs;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Oliver
 */
public class AdminUI implements Runnable {

	// reference to shared logic object
	private final CheckHunt checkHunt;

	// Swing GUI components
	private JFrame mainWindow = new JFrame("CheckHunt Control Panel");
	private JLabel flagsLbl = new JLabel("Flags: 0");
	private JLabel gameLbl = new JLabel("Game: ");
	private JButton gameOnBtn = new JButton("On");
	private JButton gameOffBtn = new JButton("Off");
	private JButton browseBtn = new JButton("Browse");
	private JFileChooser fileChooser = new JFileChooser();
	private FileFilter flagsFilter = new FileNameExtensionFilter("Flags files", "flags");
	private JLabel filePathLbl = new JLabel();
	private JPanel controlPnl = new JPanel();
	private JPanel topPnl = new JPanel();
	private JPanel gamePnl = new JPanel();
	private JPanel browsePnl = new JPanel();

	// Scoreboard
	private String[] scoreboardHeaders = new String[]{"Alias", "Score", "Submissions"};
	private Object[][] scoreboardData = new Object[][]{};
	private DefaultTableModel scoreboardDataModel = new DefaultTableModel() {
		@Override
		public String getColumnName(int col) {
			return scoreboardHeaders[col];
		}

		@Override
		public int getRowCount() {
			return scoreboardData.length;
		}

		@Override
		public int getColumnCount() {
			return scoreboardHeaders.length;
		}

		@Override
		public Object getValueAt(int row, int col) {
			return scoreboardData[row][col];
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return false;
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			scoreboardData[row][col] = value;
			fireTableCellUpdated(row, col);
		}
	};
	private JTable scoreboardTable = new JTable(scoreboardDataModel);
	private JScrollPane scoreboardScroller = new JScrollPane();
	private Contestant currentlySelectedScore = null;

	// Individual submission table
	private JDialog submissionsWindow = new JDialog(mainWindow, "Individual Submissions", true);
	// Table for displaying current listings
	private String[] submissionsHeaders = new String[]{"Num", "Submission", "Points"};
	private Object[][] submissionsData = new Object[][]{};
	private final DefaultTableModel submissionsDataModel = new DefaultTableModel() {
		@Override
		public String getColumnName(int col) {
			return submissionsHeaders[col];
		}

		@Override
		public int getRowCount() {
			return submissionsData.length;
		}

		@Override
		public int getColumnCount() {
			return submissionsHeaders.length;
		}

		@Override
		public Object getValueAt(int row, int col) {
			return submissionsData[row][col];
		}

		@Override
		public boolean isCellEditable(int row, int col) {
			return false;
		}

		@Override
		public void setValueAt(Object value, int row, int col) {
			submissionsData[row][col] = value;
			fireTableCellUpdated(row, col);
		}
	};
	private final JTable submissionsTable = new JTable(submissionsDataModel);
	private final JScrollPane submissionsScroller = new JScrollPane();

	
	
	
	
	
	private JMenuBar menuBar = new JMenuBar();
	private JMenu editMenu = new JMenu("Edit");
	private JMenuItem preferencesMenuItm = new JMenuItem("Preferences");

	
	
	
	
	
	public AdminUI(CheckHunt checkhunt) {
		this.checkHunt = checkhunt;

		// get default gui size
		int width = Prefs.DEF_GUI_WIDTH;
		int height = Prefs.DEF_GUI_HEIGHT;

		// Load remembered gui size if we should
		if (Prefs.getBoolean(Prefs.GUI_REM_SIZE, Prefs.DEF_GUI_REM_SIZE)) {
			width = Prefs.getInt(Prefs.GUI_WIDTH, width);
			height = Prefs.getInt(Prefs.GUI_HEIGHT, height);
		}

		// Set up main window
		mainWindow.setSize(width, height);
		mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainWindow.setLayout(new BorderLayout());

		// Save the window size on close
		mainWindow.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// if it's not maximised...
				if (mainWindow.getExtendedState() != JFrame.MAXIMIZED_BOTH) {
					// save the size
					Prefs.saveInt(Prefs.GUI_WIDTH, mainWindow.getWidth());
					Prefs.saveInt(Prefs.GUI_HEIGHT, mainWindow.getHeight());
				}
			}
		});

		// Setup Game on/off buttons
		setGameButtons(checkhunt.getGameActive());
		gamePnl.add(gameLbl);
		gamePnl.add(gameOnBtn);
		gamePnl.add(gameOffBtn);
		gameOnBtn.addActionListener(this::gameOnBtnListener);
		gameOffBtn.addActionListener(this::gameOffBtnListener);

		// Flags section next to game buttons
		topPnl.setLayout(new BorderLayout());
		topPnl.add(flagsLbl, BorderLayout.WEST);
		topPnl.add(gamePnl, BorderLayout.EAST);

		// Browse area
		browsePnl.setLayout(new BorderLayout());
		browsePnl.add(browseBtn, BorderLayout.WEST);
		browseBtn.addActionListener(this::browseBtnListener);
		browsePnl.add(filePathLbl);

		// Flags and game section on top of browse section 
		controlPnl.setLayout(new BorderLayout());
		controlPnl.add(topPnl, BorderLayout.NORTH);
		controlPnl.add(browsePnl, BorderLayout.SOUTH);

		mainWindow.add(controlPnl, BorderLayout.PAGE_START);

		
		
		
		
		// Menu bar
		editMenu.setMnemonic(KeyEvent.VK_E);
		editMenu.getAccessibleContext().setAccessibleDescription("Edit");
		menuBar.add(editMenu);
		
		editMenu.setMnemonic(KeyEvent.VK_P);
		preferencesMenuItm.getAccessibleContext().setAccessibleDescription("Preferences");
		preferencesMenuItm.addActionListener(this::openSettingsListener);
		editMenu.add(preferencesMenuItm);
		
		mainWindow.setJMenuBar(menuBar);
		
		
		
		
		
		// Scoreboard table
		scoreboardTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				scoreSelected();
				if (!submissionsWindow.isVisible()) {
					openSubmissionsWindow();
				}
			}
		});
		scoreboardScroller.setViewportView(scoreboardTable);
		mainWindow.add(scoreboardScroller);

		// Submissions pop out table
		submissionsWindow.setTitle("Submissions for ");
		submissionsWindow.setSize(400, 300);
		submissionsWindow.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		submissionsScroller.setViewportView(submissionsTable);
		submissionsWindow.add(submissionsScroller);

		mainWindow.setVisible(true);
	}

	@Override
	public void run() {
	}

	public void update() {
		flagsLbl.setText("Flags: " + checkHunt.getAnswers().size());
		updateScoreboard();
	}

	private void browseBtnListener(ActionEvent evt) {
		fileChooser.setFileFilter(flagsFilter);
		int fileChooserResult = fileChooser.showOpenDialog(mainWindow);
		if (fileChooserResult == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			try {
				filePathLbl.setText(file.getPath());
				checkHunt.setAnswers(CheckHunt.loadFlagsFile(file.getPath()));
			} catch (Exception e) {
				JOptionPane.showMessageDialog(mainWindow, "Could not read flag file.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private void gameOnBtnListener(ActionEvent evt) {
		try {
			checkHunt.startServer();
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(mainWindow, "Failed to start server. " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		setGameButtons(checkHunt.getGameActive());
	}

	private void gameOffBtnListener(ActionEvent evt) {
		checkHunt.stopServer();
		setGameButtons(checkHunt.getGameActive());
	}

	private void openSettingsListener(ActionEvent evt) {
		EventQueue.invokeLater(() -> {
			new SettingsUI().setVisible(true);
		});
	}

	private void updateScoreboard() {
		Object[][] tempScoreboardData = new Object[checkHunt.getScoreBoard().size()][3];
		for (int s = 0; s < checkHunt.getScoreBoard().size(); s++) {
			scoreboardData[s][0] = checkHunt.getScoreBoard().get(s).getAlias();
			scoreboardData[s][1] = checkHunt.getScoreBoard().get(s).getPoints();
			scoreboardData[s][2] = checkHunt.getScoreBoard().get(s).getSubmissions().size();
		}
		scoreboardData = tempScoreboardData;
		scoreboardDataModel.fireTableDataChanged();
	}

	private void openSubmissionsWindow() {
		submissionsWindow.setVisible(true);
	}

	private void updateSubmissionsWindow() {
		Object[][] tempSubmissionsData = new Object[checkHunt.getScoreBoard().size()][3];
		for (int s = 0; s < currentlySelectedScore.getSubmissions().size(); s++) {
			submissionsData[s][0] = s;
			submissionsData[s][0] = currentlySelectedScore.getSubmissions().get(s);
			submissionsData[s][0] = checkHunt.getPointsForSubmission(currentlySelectedScore.getSubmissions().get(s));
		}
		submissionsData = tempSubmissionsData;
		submissionsDataModel.fireTableDataChanged();
		submissionsWindow.setTitle("Submissions for " + currentlySelectedScore.getAlias());
	}

	private void scoreSelected() {
		if (scoreboardTable.getSelectedRow() < checkHunt.getScoreBoard().size()) {
			currentlySelectedScore = checkHunt.getScoreBoard().get(scoreboardTable.getSelectedRow());
		} else {
			currentlySelectedScore = null;
		}
		updateSubmissionsWindow();
	}

	private void setGameButtons(boolean gameStatus) {
		gameOnBtn.setEnabled(!gameStatus);
		gameOffBtn.setEnabled(gameStatus);
	}
}
