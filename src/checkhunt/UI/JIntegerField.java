package checkhunt.UI;

import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

/**
 *
 * @author Oliver
 */
public class JIntegerField extends JTextField {

	/**
	 * Returns the int value of the field, if the field is empty, 0 is returned
	 *
	 * @return the value of the field, or 0 if empty
	 */
	public int getInt() {
		final String text = getText();
		if (text == null || text.length() == 0) {
			return 0;
		}
		return Integer.parseInt(text);
	}

	/**
	 * Sets the value of the textbox to the given int value
	 *
	 * @param value the new value of the field
	 */
	public void setInt(int value) {
		setText(String.valueOf(value));
	}

	@Override
	protected Document createDefaultModel() {
		return new IntegerDocument();
	}

	static class IntegerDocument extends PlainDocument {

		/**
		 * This function gets called when the textbox is updated. It casts the
		 * value to an integer, if there's an exception, nothing is inserted and
		 * the device makes a 'beep' noise. If there are no exeptions, the
		 * string value is passed to the overridden function and inserted
		 * normally.
		 *
		 * @param offs the starting offset >= 0
		 * @param str the string to insert; does nothing with null/empty strings
		 * and beeps with non-integer strings
		 * @param a the attributes for the inserted content
		 *
		 * @throws BadLocationException the given insert position is not a valid
		 * position within the document
		 */
		@Override
		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
			if (str != null) {
				try {
					Integer.decode(str);
					super.insertString(offs, str, a);
				} catch (NumberFormatException e) {
					Toolkit.getDefaultToolkit().beep();
				}
			}
		}
	}
}
