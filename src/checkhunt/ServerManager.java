package checkhunt;

import java.net.ServerSocket;
import java.util.prefs.Preferences;

/**
 *
 * @author Oliver
 */
public class ServerManager {

	private final CheckHunt checkHunt;
	private Server server;
	private Thread serverThread;
	
	public ServerManager(CheckHunt checkHunt) {
		this.checkHunt = checkHunt;
	}

	protected void start() throws Exception {
		int socket = Prefs.getInt(Prefs.PORT, Prefs.DEF_PORT);
		
		server = new Server(new ServerSocket(socket),checkHunt);
		serverThread = new Thread(server);
		serverThread.start();
	}

	protected void stop() {
		if (server != null) {
			server.stopServer();
		}
	}
}
