package checkhunt;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Oliver
 */
public class Contestant {

	private String alias;
	private int points;
	private List<Submission> submissions = new ArrayList<>();

	public Contestant(String alias) {
		this.alias = alias;
	}

	public String getAlias() {
		return alias;
	}

	public int getPoints() {
		return points;
	}

	protected void addPoints(int points) {
		this.points = points;
	}

	protected void addSubmission(Submission submission) {
		submissions.add(submission);
	}

	protected int getNumSubmissions() {
		return submissions.size();
	}

	public List<Submission> getSubmissions() {
		return submissions;
	}
}
