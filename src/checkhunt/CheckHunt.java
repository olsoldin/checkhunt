package checkhunt;

import checkhunt.UI.AdminUI;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.SwingUtilities;

/**
 *
 * @author Oliver
 */
public class CheckHunt {

	public static void main(String[] args) {
		new CheckHunt();
	}

	private List<Contestant> contestants = new ArrayList<>();
	private List<Flag> correctFlags = new ArrayList<>();
	private boolean gameActive = false;

	private AdminUI gui;
	private ServerManager server;

	public CheckHunt() {
		// Initialise Interfaces
		gui = new AdminUI(this);
		server = new ServerManager(this);

		// Start GUI thread
		try {
			SwingUtilities.invokeAndWait((Runnable) gui);
		} catch (Exception ex) {
			System.err.println("Failed to start GUI thread. Exiting...");
			System.exit(1);
		}
	}

	public static List<Flag> loadFlagsFile(String path) throws Exception {
		// Load flags file, which is java properties file
		FileInputStream fis = new FileInputStream(path);
		Properties flagsFile = new Properties();
		flagsFile.load(fis);

		// Temporary flags list
		ArrayList<Flag> tempFlags = new ArrayList<>();

		// Iterate over flags in properites file
		for (Object key : flagsFile.keySet()) {
			String answer = ((String) key);
			int points = Integer.parseInt(flagsFile.getProperty(((String) key)));
			tempFlags.add(new Flag(answer, points));
		}

		return tempFlags;
	}

	public synchronized boolean getGameActive() {
		return gameActive;
	}

	public synchronized void setAnswers(List<Flag> flags) {
		correctFlags = flags;
		gui.update();
	}

	public synchronized List<Flag> getAnswers() {
		return correctFlags;
	}

	public synchronized void startServer() throws Exception {
		gameActive = true;
		server.start();
		gui.update();
	}

	public synchronized void stopServer() {
		gameActive = false;
		server.stop();
		gui.update();
	}

	public synchronized List<Contestant> getScoreBoard() {
		return contestants;
	}

	public synchronized int getPointsForSubmission(Submission submission) {
		for (Flag flag : correctFlags) {
			if (flag.getAnswer().equals(submission.getAnswer())) {
				return flag.getPoints();
			}
		}
		return 0;
	}

	protected synchronized void doSubmission(String alias, String flag) {
		Submission submission = new Submission(alias, flag);
		for (Contestant contestant : contestants) {
			if (contestant.getAlias().equals(alias)) {
				contestant.addSubmission(submission);
				contestant.addPoints(getPointsForSubmission(submission));
				return;
			}
		}

		// Didn't find an exisiting user must be new
		/* TODO: Should throw an exception */
		if (isValidAlias(alias)) {
			Contestant newContestant = new Contestant(alias);
			newContestant.addSubmission(submission);
			newContestant.addPoints(getPointsForSubmission(submission));
			contestants.add(newContestant);
		}
	}

	private boolean isValidAlias(String alias) {

		/* TODO: Add more checks, things like silly chars, html, etc.. */
		for (Contestant contestant : contestants) {
			if (contestant.getAlias().equals(alias)) {
				return false;
			}
		}

		return true;
	}
}
