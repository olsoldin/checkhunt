package checkhunt;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Paul
 */
public class Replyer implements Runnable {

	private CheckHunt checkHunt;
	private final Socket socket;
	
	private final static int MAX_REQUEST_SIZE = 1500000; //1.5MB

	public Replyer(Socket socket, CheckHunt checkHunt) {
		this.socket = socket;
		this.checkHunt = checkHunt;
	}

	@Override
	public void run() {
		try {
			InputStream inStream = socket.getInputStream();
			byte[] inDataMax = new byte[MAX_REQUEST_SIZE];
			int byteCount = inStream.read(inDataMax,0,inDataMax.length);
			byte[] inData = new byte[byteCount];			
			System.arraycopy(inDataMax, 0, inData, 0, byteCount);
			ArrayList<String> request = new ArrayList<>();
			String requestLine = "";
			for(int i = 0; i < inData.length; i++){
				char requestChar = (char)inData[i]; 
				if(requestChar == '\n'){
					request.add(requestLine);
					requestLine = "";
				}else{
					requestLine+= requestChar;
				}
			}
			request.add(requestLine);
			
			// Read Request
			RequestMethod method = getMethod(request);			
			HashMap<String,String> postData = (method == RequestMethod.POST?getPostedData(request):new HashMap<>());
			
			// Process posted data
			processPost(postData);
			
			// Reply
			sendReply("200 OK",
						"text/html",
						generatePage().getBytes(),
						(method == RequestMethod.HEAD));
		} catch (Exception ioe) {
			System.out.println("Error: Failed to process request!");
			return;
		}

	}

	enum RequestMethod {
		GET,
		HEAD,
		POST
	}
	
	private String generatePage(){		
		String page = "<html>" + 
		"<head>" +
			"<title>Check Hunt</title>" +
		"</head>" + 
		"<body>" +
			"<h1>Check Hunt - Capture the Flag Game</h1>" +
			"<h2>Make a Submission</h2>" +
			"<form method=\"POST\">" + 
				"Alias:<input type=\"text\" name=\"alias\"/>" + 
				"Flag:<input type=\"text\" name=\"flag\"/>" +
				"<button type=\"submit\">Submit</button>" + 
			"</form>" +
			"<hr/>" +
			"<h2>Scoreboard</h2>" +
			"<table align=\"center\" style=\"width:30%\">" +
			"<tr>" +
					"<th>Alias</th>" +
					"<th>Score</th>" +
			"</tr>";
		for(Contestant contestant : checkHunt.getScoreBoard()){
			page += "<tr><td>" + contestant.getAlias() + "</td>";
			page += "<td>" + contestant.getPoints() + "</td></tr>";
		}
		page+="</table>";
		return page;
	}

	private RequestMethod getMethod(ArrayList<String> request) throws Exception {
		if (request.size() > 0) {
			String methodLine = request.get(0);
			methodLine = methodLine.substring(0, 4);
			if (methodLine.equals("GET ")) {
				return RequestMethod.GET;
			} else if (methodLine.equals("HEAD")) {
				return RequestMethod.HEAD;
			} else if (methodLine.equals("POST")) {
				return RequestMethod.POST;
			} else {
				throw new HTTPErrors.NotImplementedException();
			}
		} else {
			throw new HTTPErrors.BadRequestException();
		}
	}

	private HashMap<String,String> getPostedData(ArrayList<String> request) throws Exception {
		HashMap<String,String> toReturn = new HashMap<>();
		String rawPost = request.get(request.size()-1);
		String[] rawPairs = rawPost.split("&");
		for(String rawPair : rawPairs){
			String[] keyValue = rawPair.split("=");
			if(keyValue.length != 2){throw new Exception("Bad key value pair in post.");}
			String key = keyValue[0];
			String value = keyValue[1];
			toReturn.put(key, value);
		}
		return toReturn;
	}

	private void processPost(HashMap<String,String> postData){
		String alias = "";
		String flag = "";
		for(Map.Entry<String,String> postPair : postData.entrySet()){
			if(postPair.getKey().equals("alias")){alias = postPair.getValue();}
			if(postPair.getKey().equals("flag")){flag = postPair.getValue();}
		}
		if(alias.isEmpty() || flag.isEmpty()){return;}
		checkHunt.doSubmission(alias, flag);
	}
	
	private void sendReply(String httpCode, String fileType, byte[] replyData, boolean headOnly) {
		try {
			byte[] replyPreface = ("HTTP/1.1 " + httpCode + " \r\n"
					+ "Server: CheckHuntHTTP\r\n"
					+ "Connection: close\r\n"
					+ "Content-Type: " + fileType + "\r\n"
					+ "Content-Length: " + replyData.length + "\r\n\n").getBytes();
			socket.getOutputStream().write(replyPreface);
			if (!headOnly) {
				socket.getOutputStream().write(replyData);
				socket.getOutputStream().write("\r\n\n".getBytes());
			}
		} catch (IOException ioe) {
			System.out.println("Error, failed writing to socket mid send!"
					+ " Maybe client got cut off:"
					+ "Closed=" + socket.isClosed()
					+ "Connected=" + socket.isConnected()
					+ "OutputShutdown=" + socket.isOutputShutdown());
		}
	}

}
