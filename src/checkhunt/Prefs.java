package checkhunt;

import java.util.prefs.Preferences;

/**
 * A basic wrapper for the Preferences class Doing it this way makes the
 * implementing code cleaner
 *
 * @author Oliver
 */
public class Prefs {

	// TODO: Find a better way to pass the default values for each preference
	// (possibly using enums? or maybe arrays with the index as the id and the default?)
	// Preference names
	public static final String PORT = "port";
	public static final int DEF_PORT = 80;

	public static final String GUI_REM_SIZE = "GUIrem";
	public static final boolean DEF_GUI_REM_SIZE = true;

	public static final String GUI_WIDTH = "GUIwidth";
	public static final int DEF_GUI_WIDTH = 350;

	public static final String GUI_HEIGHT = "GUIheight";
	public static final int DEF_GUI_HEIGHT = 600;

	/**
	 * Gets the preferences saved under the CheckHunt name
	 *
	 * @return Preferences object
	 */
	private static Preferences getPrefs() {
		return Preferences.userNodeForPackage(CheckHunt.class);
	}

	/**
	 * Gets the integer value stored for the given key. If the key doesn't
	 * exist, the default value is used
	 *
	 * @param key Key of the setting the be retrieved
	 * @param def Default value if the key doesn't exist
	 * @return The value stored at the location given by the key, or "def" if
	 * that key doesn't exist
	 */
	public static int getInt(String key, int def) {
		return getPrefs().getInt(key, def);
	}

	public static void saveInt(String key, int val) {
		getPrefs().putInt(key, val);
	}

	public static String getString(String key, String def) {
		return getPrefs().get(key, def);
	}

	public static void saveString(String key, String val) {
		getPrefs().put(key, val);
	}

	public static boolean getBoolean(String key, boolean def) {
		return getPrefs().getBoolean(key, def);
	}

	public static void saveBool(String key, boolean val) {
		getPrefs().putBoolean(key, val);
	}

	public static byte[] getByteArray(String key, byte[] def) {
		return getPrefs().getByteArray(key, def);
	}

	public static void saveByteArray(String key, byte[] val) {
		getPrefs().putByteArray(key, val);
	}

	public static double getDouble(String key, double def) {
		return getPrefs().getDouble(key, def);
	}

	public static void saveByteArray(String key, double val) {
		getPrefs().putDouble(key, val);
	}

	public static float getFloat(String key, float def) {
		return getPrefs().getFloat(key, def);
	}

	public static void saveFloat(String key, float val) {
		getPrefs().putFloat(key, val);
	}

	public static long getLong(String key, long def) {
		return getPrefs().getLong(key, def);
	}

	public static void saveLong(String key, long val) {
		getPrefs().putLong(key, val);
	}
}
