package checkhunt;

/**
 *
 * @author Paul
 */
public class HTTPErrors {

    public static class InternalServerErrorException extends Exception {

        public InternalServerErrorException() {
            super();
        }

        public byte[] getHTML() {
            return ("<html>"
                    + "<head><title>500 Error</title></head>"
                    + "<body>"
                    + "<h1>500 Internal Server Error</h1>"
                    + "<p>Something went wrong at our end. :(</p>"
                    + "</body>"
                    + "</html>").getBytes();
        }

        public String getStatus() {
            return "500 Internal Server Error";
        }
    }

    public static class URLToLongException extends Exception {

        public URLToLongException() {
            super();
        }

        public byte[] getHTML() {
            return ("<html>"
                    + "<head><title>414 Error</title></head>"
                    + "<body>"
                    + "<h1>414 URL Too Long</h1>"
                    + "<p>Oh no, a technical issue. :(</p>"
                    + "</body>"
                    + "</html>").getBytes();
        }

        public String getStatus() {
            return "414 URL Too Long";
        }
    }

    public static class BadRequestException extends Exception {

        public BadRequestException() {
            super();
        }

        public byte[] getHTML() {
            return ("<html>"
                    + "<head><title>400 Error</title></head>"
                    + "<body>"
                    + "<h1>400 Bad Request</h1>"
                    + "<p>Oh no, a technical issue. :(</p>"
                    + "</body>"
                    + "</html>").getBytes();
        }

        public String getStatus() {
            return "400 Bad Request";
        }
    }

    public static class NotImplementedException extends Exception {

        public NotImplementedException() {
            super();
        }

        public byte[] getHTML() {
            return ("<html>"
                    + "<head><title>501 Error</title></head>"
                    + "<body>"
                    + "<h1>501 Not Implemented</h1>"
                    + "<p>Oh no, a technical issue. :(</p>"
                    + "</body>"
                    + "</html>").getBytes();
        }

        public String getStatus() {
            return "501 Not Implemented";
        }
    }

    public static class NotFoundException extends Exception {

        public NotFoundException() {
            super();
        }

        public byte[] getHTML() {
            return ("<html>"
                    + "<head><title>404 Error</title></head>"
                    + "<body>"
                    + "<h1>404 Not Found</h1>"
                    + "<p>We can't find the page your looking for!</p>"
                    + "</body>"
                    + "</html>").getBytes();
        }

        public String getStatus() {
            return "404 Not Found";
        }
    }

}
