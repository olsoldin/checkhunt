package checkhunt;

/**
 *
 * @author Oliver
 */
public class Submission {

    private String alias;
    private String answer;

    public Submission(String alias, String answer) {
        this.alias = alias;
        this.answer = answer;
    }

    protected String getAlias() {
        return alias;
    }

    protected String getAnswer() {
        return answer;
    }
}
