package checkhunt;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Paul
 */
public class Server implements Runnable {

	private ServerSocket serverSock = null;
	private volatile boolean isRunning;
	private final CheckHunt checkHunt;

	public Server(ServerSocket serverSocket, CheckHunt checkHunt) {
		this.serverSock = serverSocket;
		this.checkHunt = checkHunt;
	}

	@Override
	public void run() {
		isRunning = true;
		while (isRunning) {
			try {
				Socket socket = serverSock.accept();
				Replyer replyer = new Replyer(socket,checkHunt);
				(replyer).run();
			} catch (IOException ioe) {
				System.out.println("Error: Creating connection" + ioe.getMessage());
			}
		}
	}
	
	protected void stopServer() {
		isRunning = false;
	}

}
